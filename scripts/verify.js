function logout(redirect) {
    var request = new XMLHttpRequest();
    var user_id = document.getElementById("user_id").innerHTML;
    request.open("GET", redirect + "login.php?action=logout&user=" + user_id, true);
    request.send();
    request.onreadystatechange = function () {
        window.location.replace(redirect);
    };
}

function verifyUser(){
    var request = new XMLHttpRequest();
    var user_id = document.getElementById("user_id").innerHTML;
    var key = document.getElementById("verify_key").value;
    request.open("GET", "../../login.php?action=verify&user=" + user_id + "&key=" + key, true);
    request.send();
    request.onreadystatechange = function () {
        console.log(request.responseText);
        if(request.responseText != "verified"){
            document.getElementById("invalid_code").style.display="inline";
        }
        else{
            window.location.replace("../")
        }
    };
}