$(document).ready(function () {
    document.getElementById("login_button").addEventListener("click", function () {
        var username = document.getElementById("login_user").value;
        var password = document.getElementById("login_pass").value;
        var request = new XMLHttpRequest();
        request.open("GET", "login.php?action=login&user=" + username + "&login=" + password, true);
        request.send();
        request.onreadystatechange = function () {
            if (request.readyState == 4 && request.status == 200) {
                if (request.responseText.toLowerCase() == "invalid_user") {
                    document.getElementById("invalid_user").style.display = "block"
                }
                else {
                    document.getElementById("invalid_user").style.display = "none";
                    window.location.replace("landing/");
                }
            }
        };
    });
    document.getElementById("link_login").addEventListener("click", toggleRegisterVisible);
    document.getElementById("register_button").addEventListener("click", registerUser);
    var register_password = document.getElementById("register_password");
    var register_repeat_password = document.getElementById("register_repeat_password");
    var validate_repeat = function () {
        if (register_password.value != register_repeat_password.value) {
            document.getElementById("password_match").style.display = "inline";
        }
        else {
            document.getElementById("password_match").style.display = "none";
        }
    };
    register_password.addEventListener("keyup", validate_repeat);
    register_repeat_password.addEventListener("keyup", validate_repeat);
});

function highlight(element, highlighted) {
    if (highlighted) {
        element.style.border = "1px solid red";
    }
    else {
        element.style.border = "1px solid gray";
    }
}

function registerUser() {
    var register_password = document.getElementById("register_password");
    var register_repeat_password = document.getElementById("register_repeat_password");
    var user_check = /^(.*[\W]+.*)+$/i;
    var email_check = /([-!#-'*+/-9=?A-Z^-~]+(\.[-!#-'*+/-9=?A-Z^-~]+)*|"([]!#-[^-~ \t]|(\\[\t -~]))+")@([-!#-'*+/-9=?A-Z^-~]+(\.[-!#-'*+/-9=?A-Z^-~]+)*|\[[\t -Z^-~]*])$/i;
    var register_ok = true;
    // Check for mismatching passwords
    if (register_password.value == "" || register_password.value != register_repeat_password.value) {
        highlight(register_password, true);
        highlight(register_repeat_password, true);
        register_ok = false;
    }
    else {
        highlight(register_password, false);
        highlight(register_repeat_password, false);
    }
    // Get username
    var username = document.getElementById("register_username").value;
    // Check for empty username
    if (!username.trim()) {
        highlight(document.getElementById("register_username"), true);
        register_ok = false;
    }
    else {
        highlight(document.getElementById("register_username"), false);
        // Check for invalid username
        if (user_check.test(username)) {
            document.getElementById("username_invalid").style.display = "inline";
            register_ok = false;
        }
        else {
            document.getElementById("username_invalid").style.display = "none";
        }
    }
    // Get password
    var password = document.getElementById("register_password").value;
    // Check for empty password
    if (!password.trim()) {
        highlight(document.getElementById("register_password"), true);
        register_ok = false;
    }
    else {
        highlight(document.getElementById("register_password"), false);
    }
    // Get first name
    var fname = document.getElementById("register_fname").value;
    // Check for empty name
    if (!fname.trim()) {
        highlight(document.getElementById("register_fname"), true);
        register_ok = false;
    }
    else {
        highlight(document.getElementById("register_fname"), false);
    }
    // Get last name
    var lname = document.getElementById("register_lname").value;
    // Check for empty name
    if (!lname.trim()) {
        highlight(document.getElementById("register_lname"), true);
        register_ok = false;
    }
    else {
        highlight(document.getElementById("register_lname"), false);
    }
    // Get email
    var email = document.getElementById("register_email").value;
    // Check for empty email
    if (!email.trim()) {
        highlight(document.getElementById("register_email"), true);
        register_ok = false;
    }
    else {
        highlight(document.getElementById("register_email"), false);
        // Check for invalid email
        if (!email_check.test(email)) {
            document.getElementById("email_invalid").style.display = "inline";
            register_ok = false;
        }
        else {
            document.getElementById("email_invalid").style.display = "none";
        }
    }

    if (!register_ok) {
        return;
    }

    var request = new XMLHttpRequest();
    request.open("GET", "login.php?action=register&user=" + username + "&login="
        + password + "&fname=" + fname + "&lname=" + lname + "&email=" + email, true);
    request.send();
    request.onreadystatechange = function () {
        document.getElementById("username_in_use").style.display = "none";
        document.getElementById("email_in_use").style.display = "none";
        if (request.responseText.toLowerCase() == "username_in_use") {
            document.getElementById("username_in_use").style.display = "inline";
        }
        else if (request.responseText.toLowerCase() == "email_in_use") {
            document.getElementById("email_in_use").style.display = "inline";
        }
        else {
            window.location.replace("account/verify");
        }
    }
}

function toggleRegisterVisible() {
    var form = document.getElementById("register_form");
    if (form.style.display == "none") {
        form.style.display = "inherit";
    }
    else {
        form.style.display = "none";
    }
}