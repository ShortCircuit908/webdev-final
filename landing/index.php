<?php
include_once "../Userdata.php";
$session_id = null;
$user_id = null;
if (isset($_COOKIE["session_id"]) && isset($_COOKIE["user_id"])) {
	$session_id = $_COOKIE["session_id"];
	$user_id = $_COOKIE["user_id"];
}
else{
	header("Location: ../");
	exit();
}
if (!Userdata::isSessionValid($session_id, $user_id)) {
	header("Location: ../");
	exit();
}
$user = Userdata::fetchById($user_id);
if(!$user["verified"]){
	header("Location: ../account/verify");
	exit();
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Final Landing</title>
		<link rel="shortcut icon" href="../images/favicon.ico"/>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"/>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css"/>
		<script src="../scripts/jquery-1.12.0.js" type="application/javascript"></script>
		<script src="../scripts/verify.js" type="application/javascript"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"
				type="application/javascript"></script>
		<link type="text/css" rel="stylesheet" href="../styles/global_styles.css">
		<link type="text/css" rel="stylesheet" href="../styles/landing_styles.css"/>
	</head>
	<body>
		<nav class="navbar navbar-default navbar fixed-top">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="#"><?php echo $user["username"] ?></a>
				</div>
				<div class="navbar-content">
					<ul class="nav navbar-nav">
						<li>
							<button class="btn btn-default navbar-btn" onclick="window.location.replace('../account')">My Account</button>
						</li>
						<li>
							<button class="btn btn-default navbar-btn active" onclick="">Landing</button>
						</li>
					</ul>
				</div>
				<div class="logout_container" style="float:right;">
					<button class="btn btn-default navbar-btn" id="logout_button" onclick="logout('../')">Sign
						Out
					</button>
				</div>
			</div>
		</nav>
		<p id="user_id" style="display: none;"><?php echo $user_id ?></p>
	</body>
	<main>

	</main>
</html>
