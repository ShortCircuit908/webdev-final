<?php
include "MyPDO.php";
class Userdata {
	public static function createUser($username, $password, $fname, $lname, $email) {
		$db = new MyPDO();
		$statement = $db->prepare("INSERT INTO `users` (`username`, `password`, `fname`, `lname`, `email`) VALUES (:username, :password, :fname, :lname, :email)");
		$statement->bindParam(":username", $username, PDO::PARAM_STR);
		//$statement->bindParam(":password", Userdata::generatePasswordHash($password), PDO::PARAM_STR);
		$statement->bindParam(":password", self::hash($password), PDO::PARAM_STR);
		$statement->bindParam(":fname", $fname, PDO::PARAM_STR);
		$statement->bindParam(":lname", $lname, PDO::PARAM_STR);
		$statement->bindParam(":email", strtolower($email), PDO::PARAM_STR);
		$statement->execute();
		$user = self::fetch($email);
		self::createEmailVerification($user);
		return $user;
	}

	public static function isSessionValid($session_id, $user_id) {
		$db = new MyPDO();
		$statement = $db->prepare("SELECT `session_id` FROM `sessions` WHERE `user_id`=:user_id");
		$statement->bindParam(":user_id", $user_id, PDO::PARAM_INT);
		$success = $statement->execute();
		return $success && $statement->fetchColumn(0) == $session_id;
	}

	public static function doVerification($user_id, $key) {
		$db = new MyPDO();
		$statement = $db->prepare("SELECT `verification_key` FROM `email_verification` WHERE `user_id`=:user_id LIMIT 1");
		$statement->bindParam(":user_id", $user_id, PDO::PARAM_INT);
		if (!$statement->execute() || $statement->rowCount() < 1) {
			return "invalid_user";
		}
		$stored_key = $statement->fetchColumn(0);
		if ($stored_key == $key) {
			/*
			$statement = $db->prepare("DELETE FROM `email_verification` WHERE `user_id`=:user_id");
			$statement->bindParam(":user_id", $user_id, PDO::PARAM_INT);
			$statement->execute();
			*/
			$statement = $db->prepare("UPDATE `users` SET `verified`=1 WHERE `id`=:user_id");
			$statement->bindParam(":user_id", $user_id, PDO::PARAM_INT);
			$statement->execute();
			return "verified";
		}
		return "invalid_key";
	}

	public static function getVerificationKey($user_id){
		$db = new MyPDO();
		$statement = $db->prepare("SELECT `verification_key` FROM `email_verification` WHERE `user_id`=:user_id");
		$statement->bindParam(":user_id", $user_id);
		$success = $statement->execute();
		return $success ? $statement->fetchColumn(0) : null;
	}

	public static function exists($username) {
		$db = new MyPDO();
		$statement = $db->prepare("SELECT COUNT(*) FROM `users` WHERE LOWER(`username`)=:username OR LOWER(`email`)=:username");
		$statement->bindParam(":username", strtolower($username), PDO::PARAM_STR);
		$success = $statement->execute();
		return $success && $statement->fetchColumn(0) > 0;
	}

	public static function fetch($username) {
		$db = new MyPDO();
		$statement = $db->prepare("SELECT * FROM `users` WHERE LOWER(`username`)=:username OR LOWER(`email`)=:username LIMIT 1");
		$statement->bindParam(":username", strtolower($username), PDO::PARAM_STR);
		$success = $statement->execute();
		$result = $statement->fetch(PDO::FETCH_ASSOC);
		if ($success && $result && sizeof($result) > 0) {
			return $result;
		}
		return null;
	}

	public static function fetchById($user_id) {
		$db = new MyPDO();
		$statement = $db->prepare("SELECT * FROM `users` WHERE `id`=:user_id");
		$statement->bindParam(":user_id", $user_id, PDO::PARAM_INT);
		$success = $statement->execute();
		$result = $statement->fetch(PDO::FETCH_ASSOC);
		if ($success && $result && sizeof($result) > 0) {
			return $result;
		}
		return null;
	}

	public static function hash($data){
		return hash("sha256", $data);
	}

	public static function createSession($session) {
		$user_id = $session[1];
		$db = new MyPDO();
		$statement = $db->prepare("DELETE FROM `sessions` WHERE `user_id`=:user_id");
		$statement->bindParam(":user_id", $user_id, PDO::PARAM_INT);
		$statement->execute();
		$statement = $db->prepare("INSERT INTO `sessions` VALUES (:user_id, :session_id)");
		$session_id = $session[0];
		$statement->bindParam(":user_id", $user_id, PDO::PARAM_INT);
		$statement->bindParam(":session_id", $session_id, PDO::PARAM_STR);
		$statement->execute();
		setcookie("session_id", $session_id, 0, "/");
		setcookie("user_id", $user_id, 0, "/");
	}

	public static function createEmailVerification($user) {
		$user_id = $user["id"];
		$user_email = $user["email"];
		$key = uniqid("", true);
		$db = new MyPDO();
		$statement = $db->prepare("DELETE FROM `email_verification` WHERE `user_id`=:user_id");
		$statement->bindParam(":user_id", $user_id, PDO::PARAM_STR);
		$statement->execute();
		$statement = $db->prepare("INSERT INTO `email_verification` VALUES (:user_id, :verification_key)");
		$statement->bindParam(":user_id", $user_id, PDO::PARAM_INT);
		$statement->bindParam(":verification_key", $key, PDO::PARAM_STR);
		$statement->execute();
		// TODO: Email verification code
		$subject = "Verify your account";

		$headers = "From: finalwebdev@milligancn.hulk.osd.wednet.edu\r\n";
		$headers .= "Reply-To: finalwebdev@milligancn.hulk.osd.wednet.edu\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

		//mail($user_email, $subject, "<html><body><p>Your verification code: " . $key . "</p></body></html>", $headers);
	}

	public static function expireSession($user_id) {
		$db = new MyPDO();
		$statement = $db->prepare("TRUNCATE `sessions`");
		$statement->bindParam(":user_id", $user_id, PDO::PARAM_INT);
		$statement->execute();
		unset($_COOKIE["user_id"]);
		unset($_COOKIE["session_id"]);
		setcookie("user_id", null, 1, "/");
		setcookie("session_id", null, 1, "/");
	}

	public static function checkPasswordValid($username, $password) {
		$db = new MyPDO();
		$statement = $db->prepare("SELECT `password` FROM `users` WHERE LOWER(`username`)=:username OR LOWER(`email`)=:username LIMIT 1");
		$statement->bindParam(':username', $username);
		$statement->execute();
		$hash = $statement->fetchColumn(0);
		return $hash === self::hash($password);
	}
}