<?php
include_once "Userdata.php";
if (!isset($_GET["action"])) {
	http_response_code(400);
	exit();
}
$action = $_GET["action"];
if ($action == "login") {
	if (!isset($_GET["user"]) || !isset($_GET["login"])) {
		http_response_code(400);
		exit();
	}
	$username = $_GET["user"];
	$password = $_GET["login"];
	if (!Userdata::exists($username) || !Userdata::checkPasswordValid($username, $password)) {
		print "invalid_user";
		exit();
	}
	$data = array(uniqid("", true), Userdata::fetch($username)["id"]);
	if (isset($_COOKIE["session_id"]) && isset($_COOKIE["user_id"])) {
		$session_id = $_COOKIE["session_id"];
		$user_id = $_COOKIE["user_id"];
		if (!Userdata::isSessionValid($session_id, $user_id)) {
			http_response_code(403);
			exit();
			//Userdata::createSession(array($session_id, $user_id));
		}
		print json_encode($data);
		exit();
	}
	Userdata::createSession($data);
	print json_encode($data);
	exit();
}
else if ($action == "verify") {
	if (!isset($_GET["user"]) || !isset($_GET["key"])) {
		http_response_code(400);
		exit();
	}
	$user_id = $_GET["user"];
	$key = $_GET["key"];
	print Userdata::doVerification($user_id, $key);
	exit();
}
else if ($action == "logout") {
	if (!isset($_GET["user"])) {
		http_response_code(400);
		exit();
	}
	Userdata::expireSession($_GET["user"]);
	exit();
}
else if ($action == "register") {
	if (!isset($_GET["user"]) || !isset($_GET["login"]) || !isset($_GET["fname"])
		|| !isset($_GET["lname"]) || !isset($_GET["email"])
	) {
		http_response_code(400);
		exit();
	}
	$username = $_GET["user"];
	$password = $_GET["login"];
	$fname = $_GET["fname"];
	$lname = $_GET["lname"];
	$email = $_GET["email"];
	if (Userdata::exists($email)) {
		print "email_in_use";
		exit();
	}
	if (Userdata::exists($username)) {
		print "username_in_use";
		exit();
	}
	$data = array(uniqid("", true), Userdata::createUser($username, $password, $fname, $lname, $email)["id"]);
	Userdata::createSession($data);
	print json_encode($data);
	exit();
}
http_response_code(400);
exit();
