CREATE DATABASE IF NOT EXISTS `webdev`;
USE `webdev`;

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id`       INT(11)      NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(64)  NOT NULL,
  `password` VARCHAR(64)  NOT NULL,
  `fname`    VARCHAR(64)  NOT NULL,
  `lname`    VARCHAR(64)  NOT NULL,
  `email`    VARCHAR(256) NOT NULL,
  `verified` BOOLEAN      NOT NULL DEFAULT FALSE,
  PRIMARY KEY (`id`),
  UNIQUE KEY (`email`),
  UNIQUE KEY (`username`)
)
  ENGINE = InnoDB, AUTO_INCREMENT = 1;

DROP TABLE IF EXISTS `email_verification`;
CREATE TABLE `email_verification` (
  `user_id`          INT(11)     NOT NULL,
  `verification_key` VARCHAR(23) NOT NULL,
  PRIMARY KEY (`user_id`)
)
  ENGINE = InnoDB;

DROP TABLE IF EXISTS `sessions`;
CREATE TABLE `sessions` (
  `user_id`    INT(11) NOT NULL,
  `session_id` VARCHAR(23),
  PRIMARY KEY (`user_id`)
)
  ENGINE = InnoDB;
