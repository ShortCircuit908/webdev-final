<?php
include_once "../Userdata.php";
$session_id = null;
$user_id = null;
if (isset($_COOKIE["session_id"]) && isset($_COOKIE["user_id"])) {
	$session_id = $_COOKIE["session_id"];
	$user_id = $_COOKIE["user_id"];
}
else {
	header("Location: ../");
	exit();
}
if (!Userdata::isSessionValid($session_id, $user_id)) {
	header("Location: ../");
	exit();
}
$user = Userdata::fetchById($user_id);
if (!$user["verified"] && isset($_GET["action"])) {
	$action = strtolower($_GET["action"]);
	if ($action == "verify") {
		echo "Normally we'd send you an email, but that's not really an option. Here's your code: ";
		echo Userdata::getVerificationKey($user_id);
	}
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Your account</title>
		<link rel="shortcut icon" href="../images/favicon.ico"/>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"/>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css"/>
		<script src="../scripts/jquery-1.12.0.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
		<script src="../scripts/verify.js" type="application/javascript"></script>
		<link type="text/css" rel="stylesheet" href="../styles/global_styles.css">
		<link type="text/css" rel="stylesheet" href="../styles/account_styles.css"/>
	</head>
	<body>
		<nav class="navbar navbar-default navbar fixed-top">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="#"><?php echo $user["username"] ?></a>
				</div>
				<div class="navbar-content">
					<ul class="nav navbar-nav">
						<li>
							<button class="btn btn-default navbar-btn active" onclick="">My Account</button>
						</li>
						<li>
							<button class="btn btn-default navbar-btn" onclick="window.location.replace('../landing')">
								Landing
							</button>
						</li>
					</ul>
				</div>
				<div class="logout_container" style="float:right;">
					<button class="btn btn-default navbar-btn" id="logout_button" onclick="logout('../')">Sign
						Out
					</button>
				</div>
			</div>
		</nav>
		<div class="container">
			<table>
				<tbody>
					<tr>
						<td>Username</td>
						<td>
							<input class="noborder" type="text" title="username" value="<?php echo $user["username"] ?>">
						</td>
					</tr>
					<tr>
						<td>Email</td>
						<td><?php echo "<input class='noborder' type='email' title='email'" . ($user["verified"] ? " disabled='disabled'" : "") . " value='" . $user["email"] . "'>" ?></td>
					</tr>
					<tr>
						<td>First name</td>
						<td>
							<input class="noborder" type="text" title="fname" value="<?php echo $user["fname"] ?>">
						</td>
					</tr>
					<tr>
						<td>Last name</td>
						<td>
							<input class="noborder" type="text" title="lname" value="<?php echo $user["lname"] ?>">
						</td>
					</tr>
					<tr>
						<td>Verified</td>
						<td><?php echo $user["verified"] ? "Yes" : "<a href='verify/?'>No</a>" ?></td>
					</tr>
					<tr>
						<td></td>
						<td>
							<button>Save changes</button>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<p id="user_id" style="display: none;"><?php echo $user_id ?></p>
	</body>
</html>
