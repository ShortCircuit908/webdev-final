<?php
include_once "../../Userdata.php";
$session_id = null;
$user_id = null;
if (isset($_COOKIE["session_id"]) && isset($_COOKIE["user_id"])) {
	$session_id = $_COOKIE["session_id"];
	$user_id = $_COOKIE["user_id"];
}
else {
	header("Location: ../../");
	exit();
}
if (!Userdata::isSessionValid($session_id, $user_id)) {
	header("Location: ../../");
	exit();
}
$user = Userdata::fetchById($user_id);
if ($user["verified"]) {
	header("Location: ../");
	exit();
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Verify your account</title>
		<link rel="shortcut icon" href="../../images/favicon.ico"/>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"/>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css"/>
		<script src="../../scripts/jquery-1.12.0.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
		<script src="../../scripts/verify.js" type="application/javascript"></script>
		<link type="text/css" rel="stylesheet" href="../../styles/global_styles.css">
		<link type="text/css" rel="stylesheet" href="../../styles/verify_styles.css"/>
	</head>
	<body>
		<div class="center_outer">
			<div class="center_middle">
				<div class="center_inner">
					<div id="verify">
						<div id="verify_form">
							<form
							">
							<h1>Verify <?php echo $user["username"] ?></h1>
							<div>
								<p>
									Enter your email verification code:
									<label>
										<input id="verify_key" type="text" placeholder="Verification code">
									</label>
									<input type="submit" value="Verify" onclick="verifyUser()">
								</p>
								<p id="invalid_code" style="color: red; display: none;;">
									Invalid code
								</p>
							</div>
							<a href="../?action=verify">I didn't get a verification code</a>
							<br>
							<br>
							<p>
								Not you? <a href="#" onclick="logout('../../')">Sign out</a>
							</p>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<p id="user_id" style="display: none;"><?php echo $user_id ?></p>
	</body>
</html>
