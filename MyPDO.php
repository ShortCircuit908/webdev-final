<?php

/**
 * Created by PhpStorm.
 * User: CMILLIGAN
 * Date: 1/25/2016
 * Time: 10:41 AM
 */
class MyPDO extends PDO{
    public function __construct(){
        try{
            parent::__construct("mysql:host=localhost;dbname=database;", "username", "password", array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
        }
        catch(PDOException $e){
            echo $e->getMessage();
        }
    }
}