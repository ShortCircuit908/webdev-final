<?php
include_once "Userdata.php";
if (isset($_COOKIE["session_id"]) && isset($_COOKIE["user_id"])) {
	$session_id = $_COOKIE["session_id"];
	$user_id = $_COOKIE["user_id"];
	if(Userdata::isSessionValid($session_id, $user_id)){
		if(Userdata::fetchById($user_id)["verified"]) {
			header("Location: landing");
		}
		else{
			header("Location: account/verify");
		}
	}
}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Final Login</title>
		<link rel="shortcut icon" href="images/favicon.ico"/>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"/>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css"/>
		<script src="scripts/jquery-1.12.0.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
		<script src="scripts/login.js"></script>
		<link type="text/css" rel="stylesheet" href="styles/global_styles.css">
		<link type="text/css" rel="stylesheet" href="styles/index_styles.css"/>
	</head>
	<body>
		<div class="center_outer">
			<div class="center_middle">
				<div class="center_inner">
					<div id="login_register">
						<div id="login_form">
							<form>
								<label title="Username">
									<input type="text" id="login_user" maxlength="64" placeholder="Username/email">
								</label>
								<label title="Password">
									<input type="password" id="login_pass" maxlength="64" placeholder="Password">
								</label>
								<input type="button" id="login_button" value="Login">
								<p id="invalid_user" style="color: red; display: none">
									Invalid username/password
								</p>
								<p id="create_account">
									Don't have an account? <a href="#" id="link_login">Create one!</a>
								</p>
							</form>
						</div>
						<div id="register_form" style="display: none;">
							<form>
								<label title="First name">
									<input type="text" id="register_fname" maxlength="64" placeholder="First name">
								</label>
								<label title="Last name">
									<input type="text" id="register_lname" maxlength="64" placeholder="Last name">
								</label>
								<div>
									<label title="Email">
										<input type="email" id="register_email" maxlength="256" placeholder="Email">
									</label>
									<p id="email_invalid" style="color:red; display: none;">
										Please enter a valid email
									</p>
									<p id="email_in_use" style="color:red; display: none;">
										This email is already registered
									</p>
								</div>
								<div>
									<label title="Username">
										<input type="text" id="register_username" maxlength="64" placeholder="Username">
									</label>
									<p id="username_invalid" style="color: red; display: none;">
										Username may contain [a-zA-Z0-9_]
									</p>
									<p id="username_in_use" style="color:red; display: none;">
										This username is already registered
									</p>
								</div>
								<label title="Password">
									<input type="password" id="register_password" maxlength="64" placeholder="Password">
								</label>
								<div>
									<label title="Repeat password">
										<input type="password" id="register_repeat_password" maxlength="64"
											   placeholder="Repeat password">
									</label>
									<p id="password_match" style="color:red; display: none;">
										The passwords don't match
									</p>
								</div>
								<input type="submit" id="register_button" value="Create user">
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
